[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/bamolisep/bamol-ls)

# BAMoL DSL VS Code Extension with Xtext Language Server

This project aims at developing a DSL for a language based on [REA](https://en.wikipedia.org/wiki/Resources,_events,_agents_(accounting_model)).

This project is based on a similar project for a Xtext VS Code extension available at [IDE extensions for the Xtext grammar language, mainly for VSCode and Theia/Gitpod](https://github.com/kuniss/xtext-ide-extensions).

## How to Install in VS Code

To use this extension without building it from source, download the vsix file from [Bitbucket](https://bitbucket.org/bamolisep/bamol-ls/downloads/). 

In VS Code, Extensions tab, in the drop down menu, select "Install from VSIX..." and select the vsix file.

You can now open or create files with the bamol extension and experiment with the language.

### Example VSCode Project

Go to the folder **bamol-project-example1** and open vscode with the following command: `code .`

The **bamol1.bamol** file contains an example of the Bamol DSL.

The Json generated files are located inside **src-gen**. 

## Build from Source Code

To build this project from source code you need to follow the next steps:

1- Build the Xtext Language Server 

- Go to the folder **bamol-language-server** and execute the following command:

    `./gradlew clean build`

2- Build the VS Code extension

- Go to the **bamol-vscode-extension** folder and execute the following commands:
    
    `npm install run-script-os`

    `npm install vsce -g`

    `npm install`

    `vsce package`
    


- You should now have a **bamol-lang-0.1.0.vsix** in the current folder.

## How to Debug

To debug the extension you should make sure that you **dot not** have the extension loaded.

### Debug the client side with VSCode

To debug the client side go to the **bamol-vscode-extension** folder and open vscode with `code .`

Set the breakpoints you wish (usually in the **client/extension.ts** file).

Select the **Debug View** and then **Start Debugging**.

### Debug the language server side with VSCode

At the moment VSCode does not support debbuging **xtend** files.

Go to the **bamol-language-server** folder and open vscode with `code .`

Set the breakpoints you wish (usually in **java** files).

Select the **Debug View** and then **Start Debugging: Debug (Attach) - Remote**.

**Note:** The client side extension starts the language server with remote debbuging active at the 8123 port. The file **.vscode/launch.json** contains the Debug configuration.

### Debug the language server side with Eclipse

With Eclipse the debug experience is complete (debbuging **xtend** files is supportted).

Open Eclipse, create a new empty workspace and select **Import**, **Existing Project into Workspace**, and then select the folder **bamol-language-server**. This should import all the contained projects as a Gradle multi-project configuration.

Set the breakpoints you wish (in **java** or **xtend** files).

Select **Run**, **Debug Configurations** and then create a new **Remote Java Application** configuration. Give it a name, select the **org.bamol.ls** project, enter **localhost** for Host and **8123** for Port. 

**Note:** The client side extension starts the language server with remote debbuging active at the 8123 port. The file **.vscode/launch.json** contains the Debug configuration.

## How to Update The Grammar

In Xtext grammars are defined in an xtext file.

To update the grammar you should update the file **Bamol.xtext** in **org.bamol.ls/src/main/java/org/bamol/ls/**.

**Note:** The grammar uses the model defined in the project **org.bamol.ls.model**, in the file **src/Bamol.xcore**. It functions as the AST (*Abstract Syntax Tree*) of the grammar.

Usually, in order to create new grammar rules you will need to update the base model.


