/*
 * generated by Xtext 2.18.0
 */
package org.bamol.ls.formatting2

import com.google.inject.Inject
import org.bamol.ls.model.Agent
import org.bamol.ls.model.BamolModel
import org.bamol.ls.services.BamolGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class BamolFormatter extends AbstractFormatter2 {
	
	@Inject extension BamolGrammarAccess

	def dispatch void format(BamolModel bamolModel, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (bamolElement : bamolModel.elements) {
			bamolElement.format
		}
	}

	def dispatch void format(Agent agent, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (feature : agent.features) {
			feature.format
		}
	}
	
	// TODO: implement for Res, Scenario, Given, Instance
}
