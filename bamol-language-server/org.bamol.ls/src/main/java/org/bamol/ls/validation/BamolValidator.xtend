/*
 * generated by Xtext 2.17.0
 */
package org.bamol.ls.validation

import org.eclipse.xtext.validation.Check
import org.bamol.ls.model.CommitDeclaration
import org.bamol.ls.model.ModelPackage

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class BamolValidator extends AbstractBamolValidator {
	
	public static val INVALID_NAME = 'invalidName'

	@Check
	def checkCommitDeclarationStartsWithCapital(CommitDeclaration commit) {
		if (!Character.isUpperCase(commit.name.charAt(0))) {
			warning('Name should start with a capital', 
					ModelPackage.Literals.BAMOL_ELEMENT__NAME,
					INVALID_NAME)
		}
	}
	
}
