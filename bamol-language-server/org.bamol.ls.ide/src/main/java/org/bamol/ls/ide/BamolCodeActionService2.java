package org.bamol.ls.ide;

import org.eclipse.emf.common.util.URI;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.CodeAction;
import org.eclipse.lsp4j.CodeActionKind;
import org.eclipse.lsp4j.CodeActionParams;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.xtext.ide.server.Document;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.bamol.ls.validation.BamolValidator;
import org.eclipse.xtext.ide.server.codeActions.ICodeActionService2;
import org.eclipse.xtext.ide.server.codeActions.ICodeActionService2.Options;

class BamolCodeActionService2 implements ICodeActionService2 {
 
//	protected def addTextEdit(WorkspaceEdit edit, URI uri, TextEdit... textEdit) {
//		edit.changes.put(uri.toString, textEdit)
//	}

	public List<Either<Command, CodeAction>> getCodeActions(final ICodeActionService2.Options options) {
		Document document = options.getDocument();
		CodeActionParams params=options.getCodeActionParams();
		XtextResource resource = options.getResource();
		ArrayList<CodeAction> result = new ArrayList<CodeAction>();
		List<Either<Command, CodeAction>> finalResult= new ArrayList<Either<Command, CodeAction>>();

		for (Diagnostic d : params.getContext().getDiagnostics()) {
			if (d.getCode().equals(BamolValidator.INVALID_NAME)) {
				String text = document.getSubstring(d.getRange());

				CodeAction codeAction=new CodeAction();
				codeAction.setKind(CodeActionKind.QuickFix);
				codeAction.setTitle("Capitalize Name");
				
				ArrayList<Diagnostic> diags=new ArrayList<Diagnostic>();
				diags.add(d);

				codeAction.setDiagnostics(diags);

				WorkspaceEdit wEdit=new WorkspaceEdit();
				TextEdit tEdit=new TextEdit();
				tEdit.setRange(d.getRange());
				tEdit.setNewText(text.substring(0, 1).toUpperCase() + text.substring(1));
				Map<String, List<TextEdit>> changes=new HashMap<String, List<TextEdit>>();
				List<TextEdit> lEdits=new ArrayList<TextEdit>();
				lEdits.add(tEdit);
				changes.put(resource.getURI().toString(), lEdits);
				wEdit.setChanges(changes);
				codeAction.setEdit(wEdit);

				finalResult.add(Either.forRight(codeAction));
			}	
		}

		return finalResult;
	}

	/*
	override getCodeActions(Options options) {
		val document = options.document
		val params = options.codeActionParams
		val resource = options.resource
		val result = <CodeAction>newArrayList
		for (d : params.context.diagnostics) {
			if (d.code == BamolValidator.INVALID_NAME) {
				val text = document.getSubstring(d.range)
				result += new CodeAction => [
					kind = CodeActionKind.QuickFix
					title = "Capitalize Name"
					diagnostics = #[d]
					// for more complex example we would use 
					// change serializer as in RenameService
					edit = new WorkspaceEdit() => [
						addTextEdit(resource.URI, new TextEdit => [
							range = d.range
							newText = text.toFirstUpper
						])
					]

				]

			}
		}
		return result.map[Either.forRight(it)]
	}
	*/

}