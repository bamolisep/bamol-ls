# BAMoL Extension for VS Code

This is a language extension for the BAMoL DSL.

**For the moment, this is an EXPERIMENTAL project**

## Features

Currrently the following features are supported:
* syntax highlighting 
* folding
* auto bracket closing and insertion
* snippets
* for file-local definitions only:
  * hover information
  * auto completion
  * jumpt to definition

## Known Issues

* There must be many, but none is yet reported here :-)

## Release Notes

* This is, for the moment, a very experimental project!

## Development Hints
This extension was initially created using [VS Code's Yeoman templates](https://code.visualstudio.com/api/get-started/your-first-extension), running `yo code` and selecting the `New Language` option.

The Textmate "grammar" was developed using the syntax highlighting grammer edior [*Iro*](https://eeyo.io/iro/) and the .plist/.json transformation tool http://json2plist.sinaapp.com/.

For working on the TextMate "grammar" please consider using the *Iro* specifcaton file ../Xtext.iro.

The language server is created using the Xtext language creation framework itself. 
